import React, { Component } from 'react';

import '../style/vendor/fontawesome-free/css/all.min.css';
import '../style/css/sb-admin-2.min.css';


class EntryDetails extends Component {
  componentDidMount() {
    this.subscribeWebsockets();
  }

  subscribeWebsockets() {
    if (!this.ws) {
      const transactionWs = new WebSocket(
        "ws://localhost:5000/transfer"
      );

      this.transactionWs = transactionWs;

      transactionWs.onopen = () => {
        console.log("connected transaction websocket");
        if (transactionWs.OPEN === transactionWs.readyState) {
          transactionWs.send('test');
        }
      };
      transactionWs.onclose = () => {
        console.log("Transaction WS Disconnected");
      };
      transactionWs.onmessage = payload => {
        const response = JSON.parse(payload.data);
        console.log("transactions", response);
      };
    } else if (this.transactionWs) {
      this.transactionWs.close();
      this.transactionWs = null;
    }
  }
  render() {
    return (
      <div>


  <div id="wrapper">

    <ul className="navbar-nav bg-gray-900 sidebar sidebar-dark accordion" id="accordionSidebar">

      <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div className="sidebar-brand-text mx-3">

          <img src={require('./img/statwig_logo.png')} alt="statwig logo" />
        </div>
      </a>

      <hr className="sidebar-divider my-0"/>

      <li className="nav-item active">
        <a className="nav-link" href="index.html">
          <i className="fas fa-fw fa-th"></i>
          <span>Overview</span></a>
      </li>


      <li className="nav-item">
        <a className="nav-link" href="shipping.html">
          <i className="fas fa-fw fa-truck"></i>
          <span>Shipping</span>
        </a>

      </li>

      <li className="nav-item">
        <a className="nav-link" href="inventory.html">
          <i className="fas fa-chart-pie"></i>
          <span>Inventory</span>
        </a>

      </li>



      <hr className="sidebar-divider"/>
      <li className="nav-item">
        <a className="nav-link" href="#">
          <i className="fas fa-fw fa-user"></i>
          <span>Profile</span></a>
      </li>

      <hr className="sidebar-divider d-none d-md-block"/>


    </ul>
    <div id="content-wrapper" className="d-flex flex-column">

      <div id="content">

        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
            <i className="fa fa-bars"></i>
          </button>

          <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div className="input-group">
              <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..."
                aria-label="Search" aria-describedby="basic-addon2"/>
              <div className="input-group-append">
                <button className="btn btn-primary" type="button">
                  <i className="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <ul className="navbar-nav ml-auto">

            <li className="nav-item dropdown no-arrow d-sm-none">
              <a className="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-search fa-fw"></i>
              </a>
              <div className="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                aria-labelledby="searchDropdown">
                <form className="form-inline mr-auto w-100 navbar-search">
                  <div className="input-group">
                    <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..."
                      aria-label="Search" aria-describedby="basic-addon2"/>
                    <div className="input-group-append">
                      <button className="btn btn-primary" type="button">
                        <i className="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <li className="nav-item dropdown no-arrow mx-1">
              <a className="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-bell fa-fw"></i>
                <span className="badge badge-danger badge-counter">3+</span>
              </a>
              <div className="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                aria-labelledby="alertsDropdown">
                <h6 className="dropdown-header">
                  Alerts Center
                </h6>
                <a className="dropdown-item d-flex align-items-center" href="#">
                  <div className="mr-3">
                    <div className="icon-circle bg-primary">
                      <i className="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div className="small text-gray-500">December 12, 2019</div>
                    <span className="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#">
                  <div className="mr-3">
                    <div className="icon-circle bg-success">
                      <i className="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div className="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#">
                  <div className="mr-3">
                    <div className="icon-circle bg-warning">
                      <i className="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div className="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a className="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>



            <div className="topbar-divider d-none d-sm-block"></div>

            <li className="nav-item dropdown no-arrow">
              <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span className="mr-2 d-none d-lg-inline text-gray-600 small">Valerie Luna</span>
                <img className="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"/>
              </a>
              <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a className="dropdown-item" href="#">
                  <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a className="dropdown-item" href="#">
                  <i className="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a className="dropdown-item" href="#">
                  <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <div className="container-fluid">

          <h1 className="h3 mb-4 text-gray-800">Enter Details</h1>
          <div className="card">
            <nav className="mt-3">
              <div className="nav nav-tabs" id="nav-tab" role="tablist">
                <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                  aria-controls="nav-home" aria-selected="true">Add Shipment</a>
                <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                  aria-controls="nav-profile" aria-selected="false">Add Inventory</a>
              </div>
            </nav>
            <div className="tab-content" id="nav-tabContent">
              <div className="tab-pane fade show active p-3" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                <form id="shipment">
                  <div className="form-row">
                    <div className="col-md-4 mb-3">
                      <label for="Shipment_Number">Shipment Number</label>
                      <input type="text" className="form-control" id="Shipment_Number" placeholder="SMT67568654956"
                        required/>
                    </div>
                    <div className="col-md-4 mb-3">
                      <label for="Client">Client Name</label>
                      <input type="text" className="form-control" id="Client" placeholder="Client Name"
                        required/>
                    </div>
                  </div>

                  <div className="form-row">
                    <div className="col-md-4 mb-3">
                      <label for="Shipment_Date">Shipment Date</label>
                      <input type="date" className="form-control" id="Shipment_Date" placeholder="dd/mm/yyy" required/>
                    </div>
                    <div className="col-md-4 mb-3">
                      <label for="Estimated_time">Estimated Arrival</label>
                      <input type="date" className="form-control" id="Estimated_time" placeholder="dd/mm/yyy" required/>
                    </div>
                  </div>
                  <hr/>
                  <h1 className="h5 mb-4 text-gray-800">Description of Goods</h1>
                  <div className="form-row">

                    <div className="col-md-6 mb-3">
                      <label for="validationDefault03">Product Type</label>
                      <select className="custom-select" required>
                        <option value="">Product Type</option>
                        <option value="1">Polio</option>
                        <option value="2">MMR</option>
                        <option value="3">Measles</option>
                      </select>
                    </div>
                    <div className="col-md-6 mb-3">
                      <label for="validationDefault04">Batch Number</label>
                      <input type="text" className="form-control" id="validationDefault04" placeholder="BTCH3456347"
                        required/>
                    </div>

                  </div>
                  <div className="form-row">

                    <div className="col-md-4 mb-3">
                      <label for="validationDefault01">Manufactured Date</label>
                      <input type="date" className="form-control" id="validationDefault01" placeholder="dd/mm/yyy" required/>
                    </div>

                    <div className="col-md-4 mb-3">
                      <label for="validationDefault01">Expiry Date</label>
                      <input type="date" className="form-control" id="validationDefault01" placeholder="dd/mm/yyy" required/>
                    </div>


                    <div className="col-md-4 mb-3">
                      <label for="validationDefault04">Quantity</label>
                      <input type="text" className="form-control" id="validationDefault04" placeholder="2454" required/>
                    </div>

                  </div>
                  <hr />


                  <button className="btn btn-success" type="submit" value="Add"><i className="fas fa-plus-circle"></i> Add New
                    Product</button>
                  <button className="btn btn-primary" type="submit" value="submit">Submit</button>
                </form>


              </div>
              <div className="tab-pane fade p-3" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                <form id="inventory">

                  <div className="form-row">

                    <div className="col-md-6 mb-3">
                      <label for="ProductType">Product Type</label>
                      <select id="ProductType" className="custom-select" required>
                        <option value="">Product Type</option>
                        <option value="Polio">Polio</option>
                        <option value="MMR">MMR</option>
                        <option value="Measles">Measles</option>
                      </select>
                    </div>
                    <div className="col-md-6 mb-3">
                      <label for="BatchNumber">Batch Number</label>
                      <input type="text" className="form-control" id="BatchNumber" placeholder="BTCH3456347" required/>
                    </div>

                  </div>
                  <div className="form-row">

                    <div className="col-md-4 mb-3">
                      <label for="ManufacturedDate">Manufactured Date</label>
                      <input type="date" className="form-control" id="ManufacturedDate" placeholder="dd/mm/yyy" required/>
                    </div>

                    <div className="col-md-4 mb-3">
                      <label for="ExpiryDate">Expiry Date</label>
                      <input type="date" className="form-control" id="ExpiryDate" placeholder="dd/mm/yyy" required/>
                    </div>


                    <div className="col-md-4 mb-3">
                      <label for="Quantity">Quantity</label>
                      <input type="text" className="form-control" id="Quantity" placeholder="2454" required/>
                    </div>

                  </div>
                  <hr />



                  <button className="btn btn-primary" type="submit" value="submit">Submit</button>
                </form>

              </div>
            </div>
          </div>
        </div>

      </div>

      <footer className="sticky-footer bg-white">
        <div className="container my-auto">
          <div className="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>

  </div>

  <a className="scroll-to-top rounded" href="#page-top">
    <i className="fas fa-angle-up"></i>
  </a>
  <div id="successmodal" className="modal fade" role='dialog'>
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="smodal-title"></h4>
            </div>
            <div className="modal-body" id= "smodal-body">
                <p></p>
                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
  </div>
  <div className="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button className="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div className="modal-footer">
          <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a className="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>


      </div>
    );
  }

}

export default EntryDetails;
