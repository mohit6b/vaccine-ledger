import React, { Component } from 'react';
//import './App.css';
import axios from 'axios';

import '../style/vendor/fontawesome-free/css/all.min.css';
import '../style/css/sb-admin-2.min.css';
import '../style/js/vectormap/jquery-jvectormap-2.0.3.css';
import '../style/vendors/morris.js/morris.css';
import '../style/css/main.css';


class Overview extends Component {

  state = { transactions: []};
  componentDidMount() {
    this.subscribeWebsockets();
    this.fetchTokens();

  }

  async fetchTokens() {
   const tokens = await  axios.get('http://localhost:5000/fetchTokensStatic');
   debugger;
   this.setState({tokens: tokens.tokens});
  }

  subscribeWebsockets() {
    if (!this.ws) {
      const transactionWs = new WebSocket(
        "ws://localhost:5000/transfer"
      );

      this.transactionWs = transactionWs;

      transactionWs.onopen = () => {
        console.log("connected transaction websocket");
        if (transactionWs.OPEN === transactionWs.readyState) {
          transactionWs.send('test');
        }
      };
      transactionWs.onclose = () => {
        console.log("Transaction WS Disconnected");
      };
      transactionWs.onmessage = async payload => {
        const response = JSON.parse(payload.data);
        console.log("transactions", response);
        this.setState({ transactions: response.message });
        const tokens = await axios.get('http://localhost:5000/fetchTokensStatic');
        debugger;
        this.setState({tokens: tokens.tokens});
      };
    } else if (this.transactionWs) {
      this.transactionWs.close();
      this.transactionWs = null;
    }
  }
  render() {
    return (
      <div>


  <div id="wrapper">

    <ul className="navbar-nav sidebar sidebar-dark accordion cust-nav-bg" id="accordionSidebar">

      <a className="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div className="sidebar-brand-text mx-3">

          <img src={require('./img/logo.png')} width="200"/>
        </div>
      </a>

      <hr className="sidebar-divider my-0"/>

      <li className="nav-item active">
        <a className="nav-link" href="#">
          <i className="fas fa-fw fa-th"></i>
          <span>Overview</span></a>
      </li>

    
      <li className="nav-item">
        <a className="nav-link" target="new" href="http://manufacturerui1.us-west-1.elasticbeanstalk.com/shipping.html">
          <i className="fas fa-fw fa-truck"></i>
          <span>Shipping</span>
        </a>
        
      </li>

      <li className="nav-item">
        <a className="nav-link" target="new" href="http://manufacturerui1.us-west-1.elasticbeanstalk.com/inventory.html">
          <i className="fas fa-chart-pie"></i>
          <span>Inventory</span>
        </a>
        
      </li>

    

      <hr className="sidebar-divider"/>
      <li className="nav-item">
        <a className="nav-link" href="#">
          <i className="fas fa-fw fa-user"></i>
          <span>Profile</span></a>
      </li>

      <hr className="sidebar-divider d-none d-md-block"/>


    </ul>

    <div id="content-wrapper" className="d-flex flex-column">

      <div id="content">

        <nav className="navbar navbar-expand topbar mb-4 static-top" style={{backgroundColor: "white"}}>

          <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
            <i className="fa fa-bars"></i>
          </button>

          <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 navbar-search">
            <div className="input-group">
              <input type="text" className="form-control bg-light border-0 small" placeholder="Search Wallet Address" aria-label="Search" aria-describedby="basic-addon2"/>
              <div className="input-group-append">
                <button className="btn btn-primary" type="button">
                  <i className="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <ul className="navbar-nav ml-auto">

            <li className="nav-item dropdown no-arrow d-sm-none">
              <a className="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-search fa-fw"></i>
              </a>
             
              
            </li>

            

            

           
          </ul>

        </nav>
        <div className="container-fluid">

          <div className="d-sm-flex align-items-center justify-content-between mb-4">
            <h6 className="h6 mb-0 cust-text" style={{fontWeight: "normal"}}>Wallet Address: <span style={{fontWeight: "lighter !important"}}>0x3f6417cD39E6595Ac06b84b2a844d9</span></h6>
          </div>

          


          <div className="row">


            <div className="col-xl-6 col-lg-6">
                <div className="card mb-4">
                  <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 className="m-0 font-weight-bold cust-text">Product's Details</h6>
                    <div className="dropdown no-arrow">
                      <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                      </a>
                      <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                        <div className="dropdown-header">Dropdown Header:</div>
                        <a className="dropdown-item" href="#">Action</a>
                        <a className="dropdown-item" href="#">Another action</a>
                        <div className="dropdown-divider"></div>
                        <a className="dropdown-item" href="#">Something else here</a>
                      </div>
                    </div>
                  </div>
                  
                  <div className="card-body">
                      <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Product Type</th>
                              <th>Balance</th>
                            </tr>
                          </thead>
                          
                          <tbody>
                            <tr>
                              <td>POLIOAXX</td>
                              <td>{this.state.tokens}</td>
                            </tr>
                            <tr>
                              <td>MMRXX</td>
                              <td>15,776</td>
                            </tr>
                            <tr>
                              <td>MMRXX</td>
                              <td>19,397</td>
                            </tr>
                             
                             
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
              </div>


            <div className="col-xl-6 col-lg-6">
              <div className="card mb-4">
                <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 className="m-0 font-weight-bold cust-text">Profile</h6>
                  <div className="dropdown no-arrow">
                    <a className="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div className="dropdown-header">Dropdown Header:</div>
                      <a className="dropdown-item" href="#">Action</a>
                      <a className="dropdown-item" href="#">Another action</a>
                      <div className="dropdown-divider"></div>
                      <a className="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <div className="card-body text-center cust-text">
                  
                    <img src={require('./img/dp.jpg')} className="img-profile rounded-circle" width="150" />
                    <p className="mt-2 mb-1">Name: Jhone Doe</p>
                    <span style={{fontWeight: "lighter"}}>Role: ERP specialist</span>
                </div>
              </div>
            </div>

            
          </div>

          <div className="row">

            <div className="col-sm-12">
           
              <nav className="mt-3">
                <div className="nav nav-tabs" id="nav-tab" role="tablist">
                  <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Recent Transactions</a>
                  <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Analytics</a>
                </div>
              </nav>
              <div className="tab-content" id="nav-tabContent">
                <div className="tab-pane bg-white fade show active p-3" style={{border:"1px solid #e3e6f0", borderTop: "none"}} id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                   
                  <div className="card-body">
                    <div className="table-responsive">
                      <table className="table table-bordered" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Transaction ID</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Value</th>
                            <th>Shipment Date</th>
                            <th>ETA</th>
                          </tr>
                        </thead>
                        
                        <tbody>
                        {this.state.transactions.map( txn => <tr>
                          <td>{txn.transactionHash}</td>
                          <td>{txn.from}</td>
                          <td>{txn.to}</td>
                          <td>{txn.value}</td>
                          <td>31/5/19</td>
                          <td>15/6/19</td>
                        </tr>)}

                           
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
    
                </div>
                <div className="tab-pane bg-white fade p-3" style={{border:"1px solid #e3e6f0",  borderTop: "none"}} id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    
                   <div className="card-body"> 
                      <div className="chart-area">
                          
                          
                          <div id="container" style={{height: "350px", minWidth: "310px"}}></div>
                          
                          
                              
                       
                          </div>
                </div> 
                </div>
              </div>
          </div>
           
          </div>

        </div>

      </div>

      <footer className="sticky-footer bg-white">
        <div className="container my-auto">
          <div className="copyright text-center my-auto">
            <span>Copyright &copy; STATWIG 2019</span>
          </div>
        </div>
      </footer>

    </div>

  </div>
  <a className="scroll-to-top rounded" href="#page-top">
    <i className="fas fa-angle-up"></i>
  </a>

  <div className="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button className="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div className="modal-footer">
          <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a className="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>




      </div>
    );
  }

}

export default Overview;
