import React, { Component } from 'react';

import '../style/vendor/fontawesome-free/css/all.min.css';
import '../style/scss/sb-admin-2.css';

class Login extends Component {
  componentDidMount() {
    this.subscribeWebsockets();
  }

  subscribeWebsockets() {
    if (!this.ws) {
      const transactionWs = new WebSocket(
        "ws://localhost:5000/transfer"
      );

      this.transactionWs = transactionWs;

      transactionWs.onopen = () => {
        console.log("connected transaction websocket");
        if (transactionWs.OPEN === transactionWs.readyState) {
          transactionWs.send('test');
        }
      };
      transactionWs.onclose = () => {
        console.log("Transaction WS Disconnected");
      };
      transactionWs.onmessage = payload => {
        const response = JSON.parse(payload.data);
        console.log("transactions", response);
      };
    } else if (this.transactionWs) {
      this.transactionWs.close();
      this.transactionWs = null;
    }
  }
  render() {
    return (
      <div>


  
            <div className="row">
              <div className="col-lg-6 d-none d-lg-block text-center" style={{background: "#444444", padding: "25%"}}>
              <img src={require('./img/statwig_logo.png')} alt="statwig logo"/>
              </div>
              <div className="col-lg-6">
                <div className="p-5 mt-5">
                  <div className="text-center">
                    <h1 className="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form className="user">
                    <div className="form-group">
                      <input type="email" className="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..."/>
                    </div>
                    <div className="form-group">
                      <input type="password" className="form-control form-control-user" id="exampleInputPassword" placeholder="Password"/>
                    </div>
                    <div className="form-group">
                      <div className="custom-control custom-checkbox small">
                        <input type="checkbox" className="custom-control-input" id="customCheck"/>
                        <label className="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <a href="index.html" className="btn btn-primary btn-user btn-block">
                      Login
                    </a>
                  </form>
                  <hr/>
                  <div className="text-center">
                    <a className="small" href="#">Forgot Password?</a>
                  </div>
                  <div className="text-center">
                    <a className="small" href="#">Create an Account!</a>
                  </div>
                </div>
              </div>
            </div>
         
        


      </div>
    );
  }

}

export default Login;
