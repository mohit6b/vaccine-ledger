import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import './App.css';
// pages
import Login from './pages/login'
import Home from './pages/home'
import Overview from './pages/overview'
import Shipping from './pages/shipping'
import Inventory from './pages/inventory'
import ShippingToTracking from './pages/shipping-to-tracking'
import InventoryToTracking from './pages/inventory-to-tracking'
import EntryDetails from './pages/entryDetails'
import Certificate from './pages/certificate'


class App extends Component {
  componentDidMount() {
    this.subscribeWebsockets();
  }

  subscribeWebsockets() {
    if (!this.ws) {
      const transactionWs = new WebSocket(
        "ws://localhost:5000/transfer"
      );

      this.transactionWs = transactionWs;

      transactionWs.onopen = () => {
        console.log("connected transaction websocket");
        if (transactionWs.OPEN === transactionWs.readyState) {
          transactionWs.send('test');
        }
      };
      transactionWs.onclose = () => {
        console.log("Transaction WS Disconnected");
      };
      transactionWs.onmessage = payload => {
        const response = JSON.parse(payload.data);
        console.log("transactions", response);
      };
    } else if (this.transactionWs) {
      this.transactionWs.close();
      this.transactionWs = null;
    }
  }
  render() {
    return (
      <div>


        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/home" component={Home} />
          <Route path="/overview" component={Overview} />
          <Route path="/shipping" component={Shipping} />
          <Route path="/inventory" component={Inventory} />
          <Route path="/entrydetails" component={EntryDetails} />
          <Route path="/shipping-to-tracking" component={ShippingToTracking} />
          <Route path="/inventory-to-tracking" component={InventoryToTracking} />
          <Route path="/certificate" component={Certificate} />

        </Switch>


      </div>
    );
  }

}

export default App;
